package com.maxim.jms.adapter;

import java.net.UnknownHostException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

@Component
public class ConsumerAdapter {

	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	
	public void sendToMongo(String json) throws UnknownHostException {
		
		logger.info("Sending to MongoDB");
		MongoClient client = new MongoClient( "localhost" , 27017 );
		
		@SuppressWarnings("deprecation")
		DB db = client.getDB("vendor");
		DBCollection collection = db.getCollection("contacts");
		logger.info("Converting JSON to DB Object");
		DBObject object = (DBObject) JSON.parse(json);
		collection.insert(object);
		logger.info("Sent to MongoDB");
	}

}
