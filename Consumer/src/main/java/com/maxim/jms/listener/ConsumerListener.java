package com.maxim.jms.listener;

import java.net.UnknownHostException;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.maxim.jms.adapter.ConsumerAdapter;

import org.apache.logging.log4j.*;

@Component
public class ConsumerListener implements MessageListener {
	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());
	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	ConsumerAdapter consumerAdapter;

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		logger.info("In OnMessage() I am here too");

		String json = null;

		if (message instanceof TextMessage) {

			try {
				json = ((TextMessage) message).getText();
				logger.info("Sending JSON to DB " + json);

				consumerAdapter.sendToMongo(json);

			} catch (JMSException e) {
				// TODO Auto-generated catch block
				logger.error("1 .Error Sending JSON to DB Message : " + json);
				jmsTemplate.convertAndSend(json);

			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				logger.error("2. Error Sending JSON to DB Message : " + json);
				jmsTemplate.convertAndSend(json);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.error("3 .Error Sending JSON to DB Message : " + json);
				jmsTemplate.convertAndSend(json);

			}
		}

	}

}
